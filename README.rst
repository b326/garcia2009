========================
garcia2009
========================

.. {# pkglts, doc

.. image:: https://b326.gitlab.io/garcia2009/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/garcia2009/1.2.0/

.. image:: https://b326.gitlab.io/garcia2009/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/garcia2009

.. image:: https://b326.gitlab.io/garcia2009/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/garcia2009/

.. image:: https://badge.fury.io/py/garcia2009.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/garcia2009

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/garcia2009/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/garcia2009/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/garcia2009/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/garcia2009/commits/main

prod: |prod_build|_ |prod_coverage|_

.. |prod_build| image:: https://gitlab.com/b326/garcia2009/badges/prod/pipeline.svg
.. _prod_build: https://gitlab.com/b326/garcia2009/commits/prod

.. |prod_coverage| image:: https://gitlab.com/b326/garcia2009/badges/prod/coverage.svg
.. _prod_coverage: https://gitlab.com/b326/garcia2009/commits/prod
.. #}

Description of BRIN model from García de Cortázar-Atauri et al. 2009

