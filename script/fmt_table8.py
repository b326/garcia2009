"""
Format default values for parameters from table8
"""
from pathlib import Path

import pandas as pd

from garcia2009 import pth_clean

df = pd.read_csv("../raw/table8.csv", sep=";", comment="#")

# normalize names according to tempo database
df['cultivar'] = df['cultivar'].replace({'Cabernet Sauvignon': 'Cabernet-Sauvignon', 'Pinot Noir': 'Pinot noir'})

df = df.set_index(['cultivar'])

# write table
df = df[sorted(df.columns)]

with open(pth_clean / "table8.csv", 'w', encoding="utf-8") as fhw:
    fhw.write(f"# This file has been generated by {Path(__file__).name}\n#\n")
    fhw.write("# cultivar: name of cultivar\n")
    fhw.write("# brin_cc: Cc parameter for BRIN model\n")
    fhw.write("# brin_gc: Gc parameter for BRIN model\n")
    fhw.write("# gdd_10_gc: Gc parameter for GDD_10 model\n")
    fhw.write("# gdd_5_gc: Gc parameter for GDD_5 model\n")
    fhw.write("# riou_gc: Gc parameter for Riou model\n")
    fhw.write("#\n")
    df.to_csv(fhw, sep=";", lineterminator="\n", float_format="%.1f")
