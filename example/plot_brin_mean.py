"""
BRUN model example
==================

Evolution of chilling and forcing units.
"""
import matplotlib.pylab as plt
import pandas as pd

from garcia2009 import formalism, params as params_mod

# read temperatures
weather = pd.read_csv("temp_histo.csv", sep=";", comment="#", parse_dates=['date'], index_col=['date'])
weather['t_min_next'] = weather['t_min'].shift(-1).fillna(method='ffill')

# read model parameters
params = params_mod.params.loc['mean'].to_dict()

# compute evolution
year = 2000  # arbitrary
day = pd.Timedelta(days=1)

# dormancy
date = pd.Timestamp(year, 8, 1)
cc = 0
records = [dict(date=date, cc=cc)]
while cc < params['c_c']:
    row = weather.loc[date]
    date += day
    cc += formalism.chilling_unit(row['t_min'], row['t_max'], params['q_10c'])
    records.append(dict(date=date, cc=cc))

df_cc = pd.DataFrame(records).set_index(['date'])

# post dormancy
gc = 0
records = [dict(date=date, gc=gc)]
while gc < params['g_c']:
    row = weather.loc[date]
    date += day
    gc += formalism.forcing_unit(row['t_min'], row['t_max'], row['t_min_next'],
                                 params['t_0bc'], params['t_mbc'])
    records.append(dict(date=date, gc=gc))

df_gc = pd.DataFrame(records).set_index(['date'])

# plot
fig, axes = plt.subplots(1, 2, figsize=(12, 5), squeeze=False)
ax = axes[0, 0]
ax.plot(df_cc.index, df_cc['cc'])

ax.set_ylabel("chilling units")

ax = axes[0, 1]
ax.plot(df_gc.index, df_gc['gc'])
ax.axvline(x=df_gc.index[-1], ls='--', color='#aaaaaa')
ax.text(df_gc.index[-1], 100, "budbreak", ha='right', va='bottom', rotation=90)

ax.set_ylabel("forcing units")

fig.tight_layout()
plt.show()
