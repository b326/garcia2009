"""
Bud break predicted by BRUN
===========================

for multiple cultivar and last 25 years.
"""
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import pandas as pd

from garcia2009 import formalism, params as params_mod

# read temperatures
weather = pd.read_csv("temp_histo.csv", sep=";", comment="#", parse_dates=['date'], index_col=['date'])
weather['t_min_next'] = weather['t_min'].shift(-1).fillna(method='ffill')

# compute bud break dates
day = pd.Timedelta(days=1)


def dormancy_ends(t0, cc_th, q_10c):
    date = t0
    cc = 0
    while cc < cc_th:
        row = weather.loc[date]
        date += day
        cc += formalism.chilling_unit(row['t_min'], row['t_max'], q_10c)

    return date


def post_dormancy_ends(t0, gc_th, t_0bc, t_mbc):
    date = t0
    gc = 0
    while gc < gc_th:
        row = weather.loc[date]
        date += day
        gc += formalism.forcing_unit(row['t_min'], row['t_max'], row['t_min_next'], t_0bc, t_mbc)

    return date


dfs = []
for variety, params_var in params_mod.params.iterrows():
    records = []

    for year in range(1980, 2005):
        dorm_break = dormancy_ends(pd.Timestamp(year, 8, 1), params_var['c_c'], params_var['q_10c'])
        bud_break = post_dormancy_ends(dorm_break, params_var['g_c'], params_var['t_0bc'], params_var['t_mbc'])
        records.append(dict(season=year + 1, dorm_break=dorm_break, bud_break=bud_break))

    dfs.append([variety, pd.DataFrame(records).set_index(['season'])])

# plot
fig, axes = plt.subplots(1, 1, figsize=(12, 7), squeeze=False)
ax = axes[0, 0]
for variety, df in dfs:
    ls = 'o-' if variety != 'mean' else 'o--'
    ax.plot(df.index, df['bud_break'].apply(lambda date: date.replace(year=2000)), ls, label=variety)

ax.legend(loc='upper center', ncol=2)
ax.set_ylabel("bud break")
ax.yaxis.set_major_formatter(mdates.DateFormatter("%m-%d"))

ax.set_xlabel("season")

fig.tight_layout()
plt.show()
